import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import * as actions from './actions'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mail: '',
      password: '',
      connected: false
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount() {
    const token = localStorage.getItem('token')
    if (token) {
      this.state.connected = true
    }
  }

  handleSubmit(e) {
    e.preventDefault()
    const { mail, password } = this.state
    const { connectUser } = this.props
    connectUser({ mail, password })
  }

  handleChange(name, value) {
    this.state[name] = value
  }

  render() {
    const { connected } = this.state
    const { user } = this.props
    if (connected || user.isAuth) {
      return <Redirect to="/" />
    }
    return (
      <div className="mainContent">
        <h1 className="title title__h1">Bienvenue</h1>
        <div className="center">
          <img alt="logo" src="src/assets/logo.svg" />
        </div>
        {
          user.errorMessage.map((error, index) => (<p key={index} className="alert alert__error s-14">{error}</p>))
        }
        <input required className="input input__login s-14" type="text" name="mail" placeholder="Email" onChange={e => this.handleChange(e.target.name, e.target.value)} />
        <input required className="input input__login s-14" type="password" name="password" placeholder="Password" onChange={e => this.handleChange(e.target.name, e.target.value)} />
        <p className="tar">
          <Link to="/forgotpassword" className="italic s-10">Mot de passe oublié ?</Link>
        </p>
        <button type="submit" className="button button__primary s-15" onClick={e => this.handleSubmit(e)}>Se connecter</button>
        <Link to="/inscription" className="button button__secondary s-15">S&apos;inscrire</Link>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps, actions)(Login)
