import React, { Component } from 'react'
import { Avatar } from '@material-ui/core'
import { AvatarGroup } from '@material-ui/lab'
import Tooltip from '@material-ui/core/Tooltip'
import Tag from '../tag'

class EventDetail extends Component {
  render() {
    return (
      <div className="detail">
        <div className="detail-header">
          <img alt="bla" src="./src/assets/event-all.jpg" />
        </div>
        <a className="back-arrow italic" href="#">Retour</a>
        <h1 className="title title__h1">Soirée année 80</h1>
        <p className="s-12 italic publicate">
          Publié par :
          <span> Suleyman Dogan</span>
        </p>
        <div className="detail-box">
          <p className="place italic">Le Globo</p>
          <p>
            <span className="date italic">29/02/2020</span>
            ,
            <span className="hour italic"> 23h00</span>
          </p>

          <p className="price italic">Gratuit</p>
        </div>
        <p>
          Fini les soirées impersonnelles et sans ambiance... Pas de boum boum qui ne font plaisir
          qu&apos;au Dj. So... DO YOU 80&apos;s ? On se replonge le temps d&apos;une nuit dans la
          folie des 80&apos;s avec tous les tubes qui ont marqués cette époque. Viens danser,
          sauter, chanter, hurler Samedi au GLOBO.
        </p>
        <h2 className="title title__h2">Participants</h2>
        <AvatarGroup>
          <Avatar alt="Remy Sharp" src="./src/assets/event-all.jpg" />
          <Avatar alt="Travis Howard" src="./src/assets/actu-all.jpg" />
          <Avatar alt="Cindy Baker" src="./src/assets/event-all.jpg" />
          <Tooltip title="Foo • Bar • Baz">
            <Avatar>+5</Avatar>
          </Tooltip>
        </AvatarGroup>
        <a className="button button__secondary s-15" href="">Je participe</a>
        <div>
          <Tag />
          <Tag />
        </div>
      </div>
    )
  }
}

export default EventDetail
