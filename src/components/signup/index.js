import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { Avatar } from '@material-ui/core'
import * as actions from './actions'

class Signup extends Component {
  constructor(props) {
    super(props)

    this.state = {
      sexe: 'F',
      firstname: null,
      lastname: null,
      mail: null,
      password: null,
      password_bis: null,
      birth_date: null,
      imgUrl: null,
      error: null,
      connected: false
    }
  }

  componentWillMount() {
    const token = localStorage.getItem('token')
    if (token) {
      this.state.connected = true
    }
  }

  handleChange(name, value, file = false) {
    if (file) {
      this.setState({ imgUrl: URL.createObjectURL(this.fileInput.files[0]) })
    } else {
      this.state[name] = value
    }
  }

  handleSubmit(e) {
    e.preventDefault()
    const {
      sexe,
      firstname,
      lastname,
      mail,
      password,
      password_bis,
      birth_date
    } = this.state
    const { connectUser } = this.props
    if (!sexe || !firstname || !lastname || !mail || !password || !password_bis || !birth_date) {
      this.setState({ error: 'Informations manquantes' })
    } else if (password !== password_bis) {
      this.setState({ error: 'Les mots de passe ne correspondent pas' })
    } else {
      const data = new FormData()
      data.append('sexe', sexe)
      data.append('firstname', firstname)
      data.append('lastname', lastname)
      data.append('mail', mail)
      data.append('password', password)
      data.append('birth_date', birth_date)
      if (this.fileInput.files[0]) {
        data.append('avatar', this.fileInput.files[0])
      }
      connectUser(data)
    }
  }

  render() {
    const { imgUrl, error, connected } = this.state
    const { user } = this.props
    if (connected || user.isAuth) {
      return <Redirect to="/" />
    }
    let showImage
    let errorHtml
    if (imgUrl) {
      showImage = (
        <div>
          <Avatar className="m-auto w-70 h-70" src={imgUrl} />
        </div>
      )
    }
    if (error) {
      errorHtml = (
        <p className="alert alert__error">{ error }</p>
      )
    }
    return (
      <div className="mainContent">
        <h1 className="title title__h1">Créer un compte</h1>
        { errorHtml }
        <br />
        <div className="d-flex jc-space-around">
          <label htmlFor="madame">
            <input required checked={true} className="input input__radio" type="radio" value="F" id="madame" name="sexe" onChange={e => this.handleChange(e.target.name, e.target.value)} />
            Madame
          </label>
          <label htmlFor="monsieur">
            <input required className="input input__radio" type="radio" value="M" id="monsieur" name="sexe" onChange={e => this.handleChange(e.target.name, e.target.value)} />
            Monsieur
          </label>
        </div>
        <input required className="input input__login s-14" type="text" name="firstname" placeholder="Nom" onChange={e => this.handleChange(e.target.name, e.target.value)} />
        <input required className="input input__login s-14" type="text" name="lastname" placeholder="Prenom" onChange={e => this.handleChange(e.target.name, e.target.value)} />
        <input required className="input input__login s-14" type="text" name="mail" placeholder="Email" onChange={e => this.handleChange(e.target.name, e.target.value)} />
        <label htmlFor="file" className="input input__login placeholder-color">
          Pourquoi pas une photo
          { showImage }
          <input className="d-none" type="file" name="avatar" id="file" ref={(ref) => { this.fileInput = ref }} onChange={e => this.handleChange(e.target.name, e.target.value, true)} />
        </label>
        <input required className="input input__login s-14" type="date" name="birth_date" placeholder="JJ/MM/AAAA" onChange={e => this.handleChange(e.target.name, e.target.value)} />
        <input required className="input input__login s-14" type="password" name="password" placeholder="Mot de passe" onChange={e => this.handleChange(e.target.name, e.target.value)} />
        <input required className="input input__login s-14" type="password" name="password_bis" placeholder="Confirmer le mot de passe" onChange={e => this.handleChange(e.target.name, e.target.value)} />
        <button type="submit" className="button button__primary s-15" onClick={e => this.handleSubmit(e)}>Suivant</button>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps, actions)(Signup)
