import axios from 'axios'
import actionsType from '../../actions/types'

export const connectUser = data => (async (dispatch) => {
  const response = await axios.post('http://localhost:4000/users/create', data)
  if (response.data.status !== 200) {
    return dispatch({
      type: actionsType.CONNEXION_FAIL,
      data: response.data.errors
    })
  }

  localStorage.setItem('token', response.data.data.token)

  return dispatch({
    type: actionsType.CONNECT_USER,
    data: response.data.data
  })
})
