import React, { Component } from 'react'
import Tag from '../tag'

class ActuDetail extends Component {
  render() {
    return (
      <div className="detail">
        <div className="detail-header">
          <img alt="bla" src="./src/assets/actu-all.jpg" />
        </div>
        <a className="back-arrow italic" href="#">Retour</a>
        <h1 className="title title__h1">L&apos;armée américaine teste la reconnaissance faciale dans le noir</h1>
        <p className="s-12 italic publicate">
          Publié par :
          <span> Suleyman Dogan</span>
        </p>
        <div className="detail-box">
          <a href="https://www.futura-sciences.com/tech/actualites/technologie-armee-americaine-teste-reconnaissance-faciale-noir-79210/" className="italic">Voir l&apos;article complet</a>
        </div>
        <p>
          Alors que l&apos;utilisation de la reconnaissance faciale fait l&apos;objet de
          polémiques en France avec la volonté du gouvernement de l&apos;expérimenter sur les
          images de vidéosurveillance, du côté des militaires son utilisation est courante sur
          les terrains de guerre. L&apos;armée américaine vient d&apos;investir dans le
          développement d&apos;une technologie de reconnaissance faciale qui fonctionnera de
          nuit et sur de longues distances.
        </p>

        <p>
          Plutôt que d&apos;exploiter des caméras classiques, ce sont des capteurs infrarouges
          qui vont être utilisés. De petite taille, en relevant les différences de température
          du visage, les capteurs permettront aux algorithmes de reconstituer et
          d&apos;identifier l&apos;individu. Le procédé sera diaboliquement efficace
          puisqu&apos;il devra fonctionner aussi bien de jour comme de nuit et même au travers
          d&apos;un pare-brise, ou du brouillard. Encore plus impressionnant, ce système sera
          aussi capable d&apos;identifier les individus jusqu&apos;à une distance 500 mètres !
        </p>
        <div>
          <Tag />
          <Tag />
        </div>
      </div>
    )
  }
}

export default ActuDetail
