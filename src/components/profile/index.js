import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Tag from '../tag'
import CardEvent from '../card-event'
import Header from '../header'

class Profile extends Component {
  render() {
    return (
      <div>
        <div className="profile">
          <Header />
          <div className="profile-header">
            <img alt="bla" src="./src/assets/event-all.jpg" />
          </div>
          <h1 className="title title__h1">Guillaume Roux</h1>

          <div className="profile-box d-flex jc-space-around">
            <div>
              <p>Rang</p>
              <p className="bold">Startuper</p>
            </div>
            <div>
              <p>Score</p>
              <p className="bold">155</p>
            </div>
          </div>
          <h2 className="title title__h2">Mes prochains événements</h2>
          <div className="d-flex jc-space-around">
            <CardEvent />
            <CardEvent />
          </div>
          <p className="tar">
            <Link to="/" className="italic s-14">Liste de mes prochains événements</Link>
          </p>
          <h2 className="title title__h2">Mes catégories</h2>
          <div>
            <Tag />
            <Tag />
          </div>
        </div>

      </div>
    )
  }
}

export default Profile
