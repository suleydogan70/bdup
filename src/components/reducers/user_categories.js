import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  errorMessage: [],
  categories: []
}

const chargeCategories = (state, action) => (
  fromJS(state)
    .setIn(['categories'], action.data)
    .setIn(['errorMessage'], [])
    .toJS()
)

const chargeCategoriesFail = (state, action) => (
  fromJS(state)
    .setIn(['categories'], [])
    .setIn(['errorMessage'], action.data)
    .toJS()
)

const user_categories = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.GET_CATEGORIES:
      return chargeCategories(state, action)
    case actionsType.GET_CATEGORIES_FAIL:
      return chargeCategoriesFail(state, action)
    default:
      return state
  }
}

export default user_categories
