import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  tag: 0
}

const updateTag = (state, action) => (
  fromJS(state)
    .setIn(['tag'], action.data)
    .toJS()
)

const currentTag = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.UPDATE_TAG:
      return updateTag(state, action)
    default:
      return state
  }
}

export default currentTag
