import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  messages: []
}

const getMessages = (state, action) => (
  fromJS(state)
    .setIn(['messages'], action.data)
    .toJS()
)

const messagerie = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.GET_MESSAGES:
      return getMessages(state, action)
    default:
      return state
  }
}

export default messagerie
