import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  events: []
}

const updateEvents = (state, action) => (
  fromJS(state)
    .setIn(['events'], action.data)
    .toJS()
)

const events = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.UPDATE_EVENTS:
      return updateEvents(state, action)
    default:
      return state
  }
}

export default events
