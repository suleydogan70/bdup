import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  user_events: []
}

const updateUserEvents = (state, action) => (
  fromJS(state)
    .setIn(['user_events'], action.data)
    .toJS()
)

const user_events = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.UPDATE_USER_EVENTS:
      return updateUserEvents(state, action)
    default:
      return state
  }
}

export default user_events
