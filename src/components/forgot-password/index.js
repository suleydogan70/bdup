import React, { Component } from 'react'

class ForgotPassword extends Component {
  render() {
    return (
      <div className="mainContent">
        <h1 className="title title__h1">Mot de passe oublié</h1>
        <div className="center">
          <img alt="forgot_password" src="src/assets/forgot-password.svg" />
        </div>
        <input required className="input input__login s-14" type="text" name="mail" placeholder="Email" />
        <button type="submit" className="button button__primary s-15">Suivant</button>
      </div>
    )
  }
}

export default ForgotPassword
