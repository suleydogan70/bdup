import axios from 'axios'
import actionsType from '../../actions/types'

export const getUser = () => (async (dispatch) => {
  const token = localStorage.getItem('token')
  const headers = {
    Authorization: `Bearer ${token}`
  }
  const response = await axios.post('http://localhost:4000/user/connect', null, { headers })
  if (response.data.status !== 200) {
    return dispatch({
      type: actionsType.CONNEXION_FAIL,
      data: response.data.errors
    })
  }

  return dispatch({
    type: actionsType.CONNECT_USER,
    data: response.data.data
  })
})

export const userPaid = id => (async (dispatch) => {
  const token = localStorage.getItem('token')
  const headers = {
    Authorization: `Bearer ${token}`
  }
  const response = await axios.post(`http://localhost:4000/users/${id}/update`, { state: 1 }, { headers })
  if (response.data.status !== 200) {
    return dispatch({
      type: actionsType.USER_PAID_FAIL,
      data: response.data.errors
    })
  }
  return dispatch({
    type: actionsType.USER_PAID,
    data: response.data.data
  })
})

export const getCategories = () => (async (dispatch) => {
  const token = localStorage.getItem('token')
  const headers = {
    Authorization: `Bearer ${token}`
  }
  const response = await axios.post('http://localhost:4000/user_categories/list', null, { headers })
  if (response.data.status !== 200) {
    return dispatch({
      type: actionsType.GET_CATEGORIES_FAIL,
      data: response.data.errors
    })
  }
  return dispatch({
    type: actionsType.GET_CATEGORIES,
    data: response.data.data
  })
})

export const getEvents = tag => (async (dispatch) => {
  const response = await axios.post('http://localhost:4000/events/list', { category: tag })
  if (response.data.status !== 200) {
    return dispatch({
      type: actionsType.UPDATE_EVENTS_FAIL,
      data: response.data.errors
    })
  }
  return dispatch({
    type: actionsType.UPDATE_EVENTS,
    data: response.data.data
  })
})

export const getUserEvents = () => (async (dispatch) => {
  const token = localStorage.getItem('token')
  const headers = {
    Authorization: `Bearer ${token}`
  }
  const response = await axios.post('http://localhost:4000/user_events/list', null, { headers })
  return dispatch({
    type: actionsType.UPDATE_USER_EVENTS,
    data: response.data.data
  })
})
