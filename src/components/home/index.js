import { PayPalButton } from 'react-paypal-button-v2'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import CardEvent from '../card-event'
import CardActu from '../card-actu'
import Categories from '../categories'
import Tag from '../tag'
import * as actions from './actions'

class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      connected: false,
      charging: false
    }
  }

  componentWillMount() {
    const token = localStorage.getItem('token')
    const {
      getUser,
      user_categories: { categories },
      getCategories,
      currentTag,
      getEvents,
      getUserEvents
    } = this.props
    if (token !== null) {
      this.state.connected = true
    }
    getUser()
    if (categories.length === 0) {
      getCategories()
    }
    getEvents(currentTag.tag)
    getUserEvents()
  }

  chargeContent() {
    this.setState({ charging: true })
  }

  render() {
    const { connected, charging } = this.state
    const {
      user: { user },
      userPaid,
      user_categories: { categories },
      currentTag,
      events,
      user_events,
      getUserEvents
    } = this.props
    if (!connected) {
      return <Redirect to="/login" />
    }
    if (categories.length === 0) {
      return <Categories />
    }
    return (
      <div>
        { user && user.state === 0 && charging ? (
          <svg className="loader" width="92" height="49" viewBox="0 0 92 49" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="14" cy="35" r="14" fill="#46CCD6" fillOpacity="0.3" />
            <circle cx="74" cy="31" r="18" fill="#46CCD6" fillOpacity="0.6" />
            <circle cx="41.5" cy="24.5" r="24.5" fill="#46CCD6" />
          </svg>
        ) : '' }
        { !user ? <p>Chargement du contenu...</p> : '' }
        <div className="o-a">
          <div className="d-flex">
            <Tag className={currentTag.tag === 0 ? 'checked' : ''} id={0} name="Tout" />
            {
              categories.map(category => (
                <Tag className={currentTag.tag === category.id ? 'checked' : ''} key={category.id} id={category.id} name={category.name} />
              ))
            }
          </div>
        </div>
        {
          user && user.state === 0
            ? (
              <div className="d-flex flex-column" id="paypalPaymentSection">
                <h1 className="title title__h1">Bienvenue sur BD&apos;UP</h1>
                <p className="center">
                  Pour accéder au contenu de notre site,
                  vous devez
                  <strong> adhérer </strong>
                  au BDE de votre école.
                </p>
                <br />
                <p className="center">
                  La somme demandée est de
                  <strong> 10 euros.</strong>
                </p>
                <br />
                <PayPalButton
                  className="paypalIframe"
                  amount="10.00"
                  options={{
                    clientId: 'AY6nKKtiUerDJE7EJnxUtDCpZ6yLfnVbcZiLufKZS11-vEMWlsD4d9m2fo29Bk0SVzzobJrM-xV4jtOC',
                    currency: 'EUR'
                  }}
                  onClick={() => this.chargeContent()}
                  onSuccess={() => {
                    userPaid(user.id)
                  }}
                  onError={() => {
                    userPaid(user.id)
                  }}
                />
              </div>
            )
            : (
              <div>
                <h1 className="title title__h1">Vous aussi, suivez le mouvement !</h1>
                <h2>Événements</h2>
                <br />
                <hr />
                <br />
                <div className="d-flex o-a">
                  {
                    events.events.map((event) => {
                      let participation = user_events.user_events.filter(ue => ue.id === event.id)
                      if (participation.length > 0) {
                        participation = true
                      } else {
                        participation = false
                      }
                      return (
                        <CardEvent
                          fct={getUserEvents}
                          key={event.id}
                          event={event}
                          participation={participation}
                        />
                      )
                    })
                  }
                </div>
                <h2>Actualités</h2>
                <br />
                <hr />
                <br />
                <div className="d-flex jc-space-around">
                  <CardActu />
                </div>
              </div>
            )
        }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    user_categories: state.user_categories,
    currentTag: state.currentTag,
    events: state.events,
    user_events: state.user_events
  }
}

export default connect(mapStateToProps, actions)(Home)
