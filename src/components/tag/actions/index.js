import actionsType from '../../actions/types'

export const updateTag = tag => (async dispatch => dispatch({
  type: actionsType.UPDATE_TAG,
  data: tag
}))

export const updateEvents = datas => (async dispatch => dispatch({
  type: actionsType.UPDATE_EVENTS,
  data: datas.data
}))
