import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import * as actions from './actions'

class Tag extends Component {
  async handleClick(id) {
    const { updateTag, updateEvents } = this.props
    document.querySelectorAll('.tag').forEach(el => el.classList.remove('checked'))
    document.getElementById(`Tag-${id}`).classList.add('checked')
    updateTag(id)
    const response = await axios.post('http://localhost:4000/events/list', { category: id })
    updateEvents(response.data)
  }

  render() {
    const { id, name, className } = this.props
    return (
      <div onClick={() => this.handleClick(id)} id={`Tag-${id}`} className={`tag ${className || ''}`}>
        <ul>
          <li><span>{ name }</span></li>
        </ul>
      </div>
    )
  }
}

export default connect(null, actions)(Tag)
