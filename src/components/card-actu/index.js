import React, { Component } from 'react'

class CardActu extends Component {
  render() {
    return (
      <div className="d-flex o-a">
        <div className="card">
          <div className="card__header">
            <img alt="bla" src="./src/assets/actu-all.jpg" />
          </div>
          <div className="card__body">
            <div className="card__body-text">
              <h3 className="title title__h3">L&apos;armée américaine teste la reconnaissance faciale dans le noir</h3>
              <a href="#">Lire la suite</a>
            </div>
          </div>
        </div>
        <div className="card">
          <div className="card__header">
            <img alt="bla" src="./src/assets/lol-actu.jpg" />
          </div>
          <div className="card__body">
            <div className="card__body-text">
              <h3 className="title title__h3">League of Legends : G2 chute, Vitality rame encore, les résultats de la semaine</h3>
              <a href="#">Lire la suite</a>
            </div>
          </div>
        </div>
        <div className="card">
          <div className="card__header">
            <img alt="bla" src="./src/assets/sport-actu.jpg" />
          </div>
          <div className="card__body">
            <div className="card__body-text">
              <h3 className="title title__h3">Antoine Dupont, demi de mêlée des Bleus : « Ça bouillonne, j&apos;ai des frissons »</h3>
              <a href="#">Lire la suite</a>
            </div>
          </div>
        </div>
        <div className="card">
          <div className="card__header">
            <img alt="bla" src="./src/assets/cuisine-actu.jpg" />
          </div>
          <div className="card__body">
            <div className="card__body-text">
              <h3 className="title title__h3">Le meilleur pizzaïolo du monde se trouve à Paris et vient d&apos;ouvrir son restaurant dans le 20e</h3>
              <a href="#">Lire la suite</a>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default CardActu
