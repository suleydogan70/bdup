import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

class CardEvent extends Component {
  async handleClick(id) {
    const { fct } = this.props
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    await axios.post('http://localhost:4000/user_events/create', { id_event: id }, { headers })
    fct()
  }

  render() {
    const { event, participation } = this.props
    let date = event.date.split('T')[0].split('-')
    let hour = event.date.split('T')[1].split(':')
    date = `${date[2]}/${date[1]}/${date[0]}`
    hour = `${hour[0]}h${hour[1]}`
    return (
      <div className="card d-flex flex-column jc-space-between">
        <div>
          <div className="card__header">
            <img alt="bla" src={`src/assets/${event.thumbnail}`} />
          </div>
          <div className="card__body">
            <div className="card__body-text">
              <h3 className="title title__h3">{ event.title }</h3>
              <p className="place s-14">{ event.address }</p>
              <p>
                <span className="date s-14">{ date }</span>
                ,&nbsp;
                <span className="hour s-14">{ hour }</span>
              </p>

              <p className="price s-14">{ event.price > 0 ? event.price : 'Gratuit' }</p>
            </div>
          </div>
        </div>
        <Link className="event_link" to="/event-detail">Voir</Link>
        <button className={`card__participate s-14 ${participation ? 'checked' : ''}`} type="button" onClick={() => this.handleClick(event.id)}>Je participe</button>
      </div>
    )
  }
}

export default CardEvent
