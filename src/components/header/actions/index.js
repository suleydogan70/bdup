import actionsType from '../../actions/types'

export const updateMessage = datas => (async dispatch => dispatch({
  type: actionsType.GET_MESSAGES,
  data: datas.data
}))
