import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Avatar } from '@material-ui/core'
import axios from 'axios'
import * as actions from './actions'
import Message from './components'

class Header extends Component {
  constructor(props) {
    super(props)

    this.state = {
      categories: [{
        name: 'General',
        img: 'general',
        id: 0
      }, {
        name: 'Cuisine',
        img: 'cuisines',
        id: 1
      }, {
        name: 'Sport',
        img: 'sport',
        id: 2
      }, {
        name: 'Audio visuel',
        img: 'audiovisuel',
        id: 3
      }, {
        name: 'Voyage',
        img: 'voyages',
        id: 4
      }, {
        name: 'Langues',
        img: 'langues',
        id: 5
      }, {
        name: 'Soirées',
        img: 'soiree',
        id: 6
      }, {
        name: 'Jeux vidéos',
        img: 'jeuxvideos',
        id: 7
      }, {
        name: 'Musiques',
        img: 'musiques',
        id: 8
      }],
      message: ''
    }
  }

  toggleMenu() {
    document.querySelector('.nav').classList.toggle('active')
  }

  async toggleMessagerie() {
    const { currentTag, updateMessage } = this.props
    document.querySelector('.messagerie').classList.toggle('active')
    const response = await axios.post('http://localhost:4000/messages/list', { id_category: currentTag.tag })
    updateMessage(response.data)
  }

  handleChange(value) {
    this.state.message = value
  }

  async handleSubmit(e) {
    e.preventDefault()
    const { message } = this.state
    const { user, currentTag, updateMessage } = this.props
    const response = await axios.post('http://localhost:4000/messages/create', { id_category: currentTag.tag, id_user: user.user.id, message })
    updateMessage(response.data)
    document.getElementById('messageSender').value = ''
  }

  render() {
    const { user, currentTag, messagerie } = this.props
    const { categories } = this.state
    const category = categories.filter(cat => cat.id === currentTag.tag)[0]
    return (
      <div>
        <div className="header">
          <div className="header-avatar">
            <Avatar onClick={() => this.toggleMenu()} src={`http://localhost:4000/medias/avatars/${user.user ? user.user.avatar : 'avatar_default.png'}`} />
          </div>
          <span className="toChat" href="/chat" onClick={() => this.toggleMessagerie()}>
            <img alt="" src="src/assets/chat.svg" />
          </span>
        </div>
        <nav className="nav">
          <Avatar className="m-auto w-70 h-70" src={`http://localhost:4000/medias/avatars/${user.user ? user.user.avatar : 'avatar_default.png'}`} />
          <br />
          <p className="bold center">{ user.user ? user.user.fullname : 'Chargement...' }</p>
          <div className="d-flex jc-center">
            <br />
            <p className="center">
              { user.user ? user.user.name : 'Chargement...' }
            </p>
          </div>
          <ul>
            <li className="active"><a className="s-20" href="/profile">Mon compte</a></li>
            <li><a className="s-20">Ma boîte à idée</a></li>
            <li><a className="s-20">Mes prochains événements</a></li>
            <li><a className="s-20">Me déconnecter</a></li>
          </ul>
        </nav>
        <div className="messagerie">
          <h2 className="title">{`#${category.name}`}</h2>
          <hr />
          {
            messagerie.messages.map(message => (
              <Message user={user.user.id} key={message.content} text={message} />
            ))
          }
          <form onSubmit={e => this.handleSubmit(e)}>
            <input id="messageSender" type="text" className="input input__login" placeholder="Ecrivez un message..." onChange={e => this.handleChange(e.target.value)} />
          </form>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    currentTag: state.currentTag,
    messagerie: state.messagerie
  }
}

export default connect(mapStateToProps, actions)(Header)
