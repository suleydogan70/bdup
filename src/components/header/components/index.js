import React, { Component } from 'react'
import { Avatar } from '@material-ui/core'

class Message extends Component {
  render() {
    const { text, user } = this.props
    return (
      <div>
        <br />
        <div className="message d-flex">
          <Avatar
            style={{
              width: '28px',
              height: '28px'
            }}
            src={`http://localhost:4000/medias/avatars/${text.avatar ? text.avatar : 'avatar_default.png'}`}
          />
          &nbsp;
          <p className={user === text.id_user ? 'tar' : ''}><span title={text.fullname}>{ text.content }</span></p>
        </div>
      </div>
    )
  }
}

export default Message
