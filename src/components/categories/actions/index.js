import actionsType from '../../actions/types'

export const sendCategories = datas => (async (dispatch) => {
  if (datas.status !== 200) {
    return dispatch({
      type: actionsType.GET_CATEGORIES_FAIL,
      data: datas.errors
    })
  }
  return dispatch({
    type: actionsType.GET_CATEGORIES,
    data: datas.data
  })
})
