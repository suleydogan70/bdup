import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import * as actions from './actions'

class Categories extends Component {
  constructor(props) {
    super(props)

    this.state = {
      counter: 5,
      categories: [{
        name: 'Cuisine',
        img: 'cuisines',
        id: 1,
        checked: false
      }, {
        name: 'Sport',
        img: 'sport',
        id: 2,
        checked: false
      }, {
        name: 'Audio visuel',
        img: 'audiovisuel',
        id: 3,
        checked: false
      }, {
        name: 'Voyage',
        img: 'voyages',
        id: 4,
        checked: false
      }, {
        name: 'Langues',
        img: 'langues',
        id: 5,
        checked: false
      }, {
        name: 'Soirées',
        img: 'soiree',
        id: 6,
        checked: false
      }, {
        name: 'Jeux vidéos',
        img: 'jeuxvideos',
        id: 7,
        checked: false
      }, {
        name: 'Musiques',
        img: 'musiques',
        id: 8,
        checked: false
      }]
    }
  }

  handleClick(id) {
    const { categories } = this.state
    let { counter } = this.state
    if (categories[id - 1].checked) {
      counter += 1
    }
    if (counter !== 0) {
      if (!categories[id - 1].checked) {
        counter -= 1
      }
      categories[id - 1].checked = !categories[id - 1].checked
      this.setState({ categories, counter })
    }
  }

  async handleSubmit() {
    const { categories } = this.state
    const { sendCategories } = this.props
    const ids = categories.filter(category => category.checked)
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    const response = await axios.post('http://localhost:4000/user_categories/create', ids, { headers })
    sendCategories(response.data)
  }

  render() {
    const { categories, counter } = this.state
    return (
      <div>
        <h2>{ `Choisissez ${counter} clubs qui vous plaisent` }</h2>
        <div className="d-grid">
          {
            categories.map(category => (
              <div key={category.id} className={`d-flex flex-column categories ${category.checked ? 'checked' : ''}`} onClick={() => this.handleClick(category.id)}>
                <img src={`src/assets/${category.img}.svg`} alt="Cuisine" />
                <br />
                <p className="center bold">{ category.name }</p>
              </div>
            ))
          }
        </div>
        {
          counter !== 5
            ? <button type="button" className="button button__primary" onClick={() => this.handleSubmit()}>Valider</button>
            : ''
        }
      </div>
    )
  }
}

export default connect(null, actions)(Categories)
