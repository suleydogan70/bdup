import { combineReducers } from 'redux'
import user from './components/reducers/user'
import user_categories from './components/reducers/user_categories'
import currentTag from './components/reducers/currentTag'
import events from './components/reducers/events'
import user_events from './components/reducers/user_events'
import messagerie from './components/reducers/messagerie'

export default combineReducers({
  user,
  user_categories,
  currentTag,
  events,
  user_events,
  messagerie
})
