import React, { Component } from 'react'
import {
  BrowserRouter,
  Route,
  Switch,
  Redirect
} from 'react-router-dom'

import Header from './components/header'
import Home from './components/home'
import Login from './components/login'
import Signup from './components/signup'
import ForgotPassword from './components/forgot-password'
import ActuDetail from './components/actu-detail'
import EventDetail from './components/event-detail'
import Profile from './components/profile'

class WithHeader extends Component {
  constructor(props) {
    super(props)

    this.state = {
      connected: false
    }
  }

  componentWillMount() {
    const token = localStorage.getItem('token')
    if (token) {
      this.state.connected = true
    }
  }

  render() {
    const { connected } = this.state
    if (!connected) {
      return <Redirect to="/login" />
    }
    return (
      <div>
        <Header />
        <div className="mainContent">
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/actu-detail" component={ActuDetail} exact />
            <Route path="/event-detail" component={EventDetail} exact />
            <Route path="/actu-detail" component={ActuDetail} exact />
            <Route path="/event-detail" component={EventDetail} exact />
            <Route path="/profile" component={Profile} exact />
          </Switch>
        </div>
      </div>
    )
  }
}

class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <main>
            <Switch>
              <Route path="/login" component={Login} exact />
              <Route path="/inscription" component={Signup} exact />
              <Route path="/forgotpassword" component={ForgotPassword} exact />
              <Route path="/actu-detail" component={ActuDetail} exact />
              <Route path="/event-detail" component={EventDetail} exact />
              <Route path="/profile" component={Profile} exact />
              <Route component={WithHeader} />
            </Switch>
          </main>
        </div>
      </BrowserRouter>
    )
  }
}

export default Routes
